.PHONY: dev-up

dev-up:
	@ docker-compose up -d
	@ docker-compose run --rm php composer.phar install

dev-build:
	@ docker-compose down
	@ docker volume prune -f
	@ docker-compose up -d --build --remove-orphans
	@ make dev-up

dev-restart:
	@ docker-compose restart

dev-down:
	@ docker-compose down
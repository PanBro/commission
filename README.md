**Commands**

You should execute command from the project root:

`make dev-up` - build project
`make dev-build` - re-build project

`docker exec -it commissions_php_1 bash` - to enter the container

`bin/console calculate:commissions input.txt` - to execute calculation command

`composer.phar run test -- --group=commissions-service` - run tests

web address: localhost:90

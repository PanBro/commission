<?php

namespace App\Exceptions;

class InvalidJsonDecodeResultException extends \RuntimeException
{
    /** @var string  */
    protected $message = 'An error occurred while executing the json_decode function';
}
<?php

namespace App\Utils\Writers;

class ConsoleWriter implements WriterInterface
{
    /**
     * @param  float  $resultString
     */
    public function write(float $resultString) : void
    {
        echo $resultString . PHP_EOL;
    }
}
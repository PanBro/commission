<?php

namespace App\Utils\Writers;

use Generator;

interface WriterInterface
{
    /**
     * @param  float  $resultString
     */
    public function write(float $resultString) : void;
}
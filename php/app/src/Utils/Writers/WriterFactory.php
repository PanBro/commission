<?php

namespace App\Utils\Writers;

use App\Utils\Commissions\Exceptions\ParameterForFactoryException;

class WriterFactory
{
    /** @var string */
    private $writerClass;

    public function __construct(string $writerClass)
    {
        $this->writerClass = $writerClass;
    }

    /**
     * @return WriterInterface
     */
    public function getWriterClass() : WriterInterface
    {
        if ($this->writerClass === 'to_console') {
            return new ConsoleWriter();
        }

        throw new ParameterForFactoryException('Can\'t build parameter for WriterFactory class');
    }
}
<?php

namespace App\Utils\Readers;

use Generator;

class FileFromDiskParser implements ParserInterface
{
    private const PATH = '/var/www/uploads/';
    /** @var string */
    private $fileName;

    /**
     * @param  string  $fileName
     */
    public function setFileName(string $fileName) : void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return Generator
     */
    public function parse() : Generator
    {
        return $this->openFileLineByLine(self::PATH . $this->fileName);
    }

    /**
     * @param  string  $path
     *
     * @return Generator
     */
    private function openFileLineByLine(string $path) : Generator
    {
        $openFileInArray = file($path);
        foreach ($openFileInArray as $oneLine) {
            yield $oneLine;
        }
    }
}
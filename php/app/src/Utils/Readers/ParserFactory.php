<?php

namespace App\Utils\Readers;

use App\Utils\Commissions\Exceptions\ParameterForFactoryException;

class ParserFactory
{
    /** @var string */
    private $readerClass;

    public function __construct(string $readerClass)
    {
        $this->readerClass = $readerClass;
    }

    /**
     * @return ParserInterface
     */
    public function getParserObject() : ParserInterface
    {
        if ($this->readerClass === 'from_disk') {
            return new FileFromDiskParser();
        }

        throw new ParameterForFactoryException('Can\'t build parameter for ParserFactory class');
    }
}
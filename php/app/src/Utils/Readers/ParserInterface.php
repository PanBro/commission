<?php

namespace App\Utils\Readers;

use Generator;

interface ParserInterface
{
    /**
     * @return Generator
     */
    public function parse() : Generator;

    /**
     * @param  string  $name
     */
    public function setFileName(string $name) : void;
}
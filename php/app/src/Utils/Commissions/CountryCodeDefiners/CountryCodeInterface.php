<?php

namespace App\Utils\Commissions\CountryCodeDefiners;

interface CountryCodeInterface
{
    /**
     * @param  int  $bin
     *
     * @return string
     */
    public function getCountryCode(int $bin) : string;
}
<?php

namespace App\Utils\Commissions\CountryCodeDefiners;

use App\Exceptions\InvalidResourceException;

class LookupBinList implements CountryCodeInterface
{
    private const RESOURCE = 'https://lookup.binlist.net/';

    /**
     * @param  int  $bin
     *
     * @return string
     */
    public function getCountryCode(int $bin) : string
    {
        $cardInfo = $this->getResourceContentAsArray($bin);

        return $this->findCode($cardInfo);
    }

    /**
     * @param string $bin
     *
     * @return array
     */
    private function getResourceContentAsArray(string $bin) : array
    {
        return json_decode($this->getResourceContent($bin), true);
    }

    /**
     * @param  string  $bin
     *
     * @return string
     */
    private function getResourceContent(string $bin) : string
    {
        $urlHeaders = get_headers(self::RESOURCE . $bin, 1);

        if (!array_key_exists('Content-Type', $urlHeaders)
            || !$info = file_get_contents(self::RESOURCE . $bin)) {
            throw new InvalidResourceException("Can't get card info, the resource is invalid");
        }

        return $info;
    }

    /**
     * @param array $cardInfo
     *
     * @return string
     */
    private function findCode(array $cardInfo) : string
    {
        if (!array_key_exists('country', $cardInfo) || !array_key_exists('alpha2', $cardInfo['country'])) {
            throw new InvalidResourceException("Can't get the country code, the response from the resource is invalid");
        }

        return $cardInfo['country']['alpha2'];
    }
}
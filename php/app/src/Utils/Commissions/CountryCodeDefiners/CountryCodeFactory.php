<?php

namespace App\Utils\Commissions\CountryCodeDefiners;

class CountryCodeFactory
{
    /** @var string */
    private $countryCodeDefinerClass;

    public function __construct(string $countryCodeDefinerClass)
    {
        $this->countryCodeDefinerClass = $countryCodeDefinerClass;
    }

    /**
     * @return CountryCodeInterface
     */
    public function getCountryCodeDefinerClass() : CountryCodeInterface
    {
        return new $this->countryCodeDefinerClass;
    }
}
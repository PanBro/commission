<?php

namespace App\Utils\Commissions;

class EuroUnionCountryCodeList
{
    /**
     * @var string[]
     */
    private $codes = [
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'ES',
        'FI',
        'FR',
        'GR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LT',
        'LU',
        'LV',
        'MT',
        'NL',
        'PO',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK',
    ];

    /**
     * @param string $code
     *
     * @return bool
     */
    public function has(string $code) : bool
    {
        return in_array($code, $this->codes, true);
    }
}
<?php

namespace App\Utils\Commissions;

use App\Utils\Commissions\Calculations\CommissionsCalculator;
use App\Utils\Readers\ParserInterface;
use App\Utils\Writers\WriterInterface;

class CommissionsService
{
    /** @var ParserInterface */
    private $parser;
    /** @var WriterInterface */
    private $writer;
    /** @var CommissionsCalculator */
    private $calculator;

    public function calculateCommissions() : void
    {
        $content = $this->parser->parse();

        foreach ($content as $item) {
            $commission = $this->calculator->calculate($item);

            $this->writer->write($commission);
        }
    }

    /**
     * @param  ParserInterface  $parser
     */
    public function setParser(ParserInterface $parser) : void
    {
        $this->parser = $parser;
    }

    public function setWriter(WriterInterface $writer) : void
    {
        $this->writer = $writer;
    }

    public function setCalculator(CommissionsCalculator $calculator) : void
    {
        $this->calculator = $calculator;
    }
}
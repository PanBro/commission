<?php

namespace App\Utils\Commissions\Calculations;

interface CalculatorInterface
{
    /**
     * @param  string  $item
     *
     * @return string
     */
    public function calculate(string $item) : string;
}
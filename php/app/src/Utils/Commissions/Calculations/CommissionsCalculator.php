<?php

namespace App\Utils\Commissions\Calculations;

use App\Exceptions\InvalidJsonDecodeResultException;
use App\Utils\Commissions\CountryCodeDefiners\CountryCodeInterface;
use App\Utils\Commissions\CurrencyRateDefiners\RatesInterface;
use App\Utils\Commissions\EuroUnionCountryCodeList;

class CommissionsCalculator implements CalculatorInterface
{
    private const EU_COMMISSION_PERCENTAGE = 0.01;
    private const NON_EU_COMMISSION_PERCENTAGE = 0.02;

    /** @var RatesInterface */
    private $rateDefiner;
    /** @var CountryCodeInterface */
    private $countryCodeDefiner;
    /** @var EuroUnionCountryCodeList */
    private $euCountryList;

    public function __construct()
    {
        $this->euCountryList = new EuroUnionCountryCodeList();
    }

    public function calculate(string $item) : string
    {
        $transaction = json_decode($item, true);
        if (!is_array($transaction)) {
            throw new InvalidJsonDecodeResultException();
        }
        $bin = (int)$this->getValueByKey($transaction, 'bin');
        $amount = (float)$this->getValueByKey($transaction, 'amount');
        $currency = $this->getValueByKey($transaction, 'currency');

        $rate = $this->rateDefiner->getEuroRate($currency);
        $amountInEur = $this->calculateAmountInEur($amount, $rate);

        $countryCode = $this->countryCodeDefiner->getCountryCode($bin);
        $countryIsInEU = $this->isInEU($countryCode);

        return $this->calculateCommissions($countryIsInEU, $amountInEur);
    }

    /**
     * @param  CountryCodeInterface  $countryCodeDefiner
     */
    public function setCodeDefiner(CountryCodeInterface $countryCodeDefiner) : void
    {
        $this->countryCodeDefiner = $countryCodeDefiner;
    }

    /**
     * @param  RatesInterface  $rateDefiner
     */
    public function setRateDefiner(RatesInterface $rateDefiner) : void
    {
        $this->rateDefiner = $rateDefiner;
    }

    /**
     * @param  array   $transaction
     * @param  string  $key
     *
     * @return string
     */
    private function getValueByKey(array $transaction, string $key) : string
    {
        if (!array_key_exists($key, $transaction)) {
            throw new \RuntimeException(sprintf('Transaction has no "%s" code', $key));
        }

        return $transaction[$key];
    }

    /**
     * @param string $code
     *
     * @return bool
     */
    private function isInEU(string $code) : bool
    {
        return $this->euCountryList->has($code);
    }

    /**
     * @param  bool   $isEU
     * @param  float  $amount
     *
     * @return float
     */
    private function calculateCommissions(bool $isEU, float $amount) : float
    {
        $commissionInPercents = $isEU ? self::EU_COMMISSION_PERCENTAGE : self::NON_EU_COMMISSION_PERCENTAGE;

        return ceil($amount * $commissionInPercents * 100) / 100;
    }

    /**
     * @param  float  $amount
     * @param  float  $rate
     *
     * @return float
     */
    private function calculateAmountInEur(float $amount, float $rate) : float
    {
        return $amount / $rate;
    }
}
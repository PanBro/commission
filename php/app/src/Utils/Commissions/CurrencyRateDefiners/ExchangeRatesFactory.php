<?php

namespace App\Utils\Commissions\CurrencyRateDefiners;

class ExchangeRatesFactory
{
    /** @var string */
    private $rates;

    public function __construct(string $rates)
    {
        $this->rates = $rates;
    }

    /**
     * @return RatesInterface
     */
    public function getExchangeClass() : RatesInterface
    {
        return new $this->rates;
    }
}
<?php

namespace App\Utils\Commissions\CurrencyRateDefiners;

use App\Exceptions\InvalidResourceException;
use App\Utils\Commissions\Exceptions\CurrencyRateException;

class ExchangeRatesApi implements RatesInterface
{
    private const RESOURCE = 'https://api.exchangeratesapi.io/latest';
    private const EUR_TO_EUR_COURSE = 1.00;

    /**
     * @param string $currency
     *
     * @return float
     */
    public function getEuroRate(string $currency) : float
    {
        $exchangeCourses = $this->getResourceContentAsArray();

        return $this->findCourse($exchangeCourses, $currency);
    }

    /**
     * @return array
     */
    private function getResourceContentAsArray() : array
    {
        return json_decode($this->getResourceContent(), true);
    }

    /**
     * @return string
     */
    private function getResourceContent() : string
    {
        $urlHeaders = get_headers(self::RESOURCE, 1);

        if (!array_key_exists('Content-Type', $urlHeaders)
            || !$info = file_get_contents(self::RESOURCE )) {
            throw new InvalidResourceException('Can\'t get currencies list, the resource is invalid');
        }

        return $info;
    }

    /**
     * @param  array   $exchangeCourses
     * @param  string  $currency
     *
     * @return float
     */
    private function findCourse(array $exchangeCourses, string $currency) : float
    {
        if ($currency === 'EUR') {
            return self::EUR_TO_EUR_COURSE;
        }
        if (!array_key_exists('rates', $exchangeCourses)) {
            throw new InvalidResourceException("Can't get the currency exchange rate, the response from the resource is invalid");
        }
        if (!array_key_exists($currency, $exchangeCourses['rates']) && $currency !== 'EUR') {
            throw new CurrencyRateException(sprintf('Can\'t define currency rate for %s', $currency));
        }

        return $exchangeCourses['rates'][$currency];
    }
}
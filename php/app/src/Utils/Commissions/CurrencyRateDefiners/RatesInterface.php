<?php

namespace App\Utils\Commissions\CurrencyRateDefiners;

interface RatesInterface
{
    /**
     * @param string $currency
     *
     * @return float
     */
    public function getEuroRate(string $currency) : float;
}
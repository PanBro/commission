<?php

namespace App\Command;

use App\Utils\Commissions\Calculations\CommissionsCalculator;
use App\Utils\Commissions\CommissionsService;
use App\Utils\Commissions\CurrencyRateDefiners\{ExchangeRatesFactory, RatesInterface};
use App\Utils\Commissions\CountryCodeDefiners\{CountryCodeFactory, CountryCodeInterface};
use App\Utils\Readers\{ParserInterface, ParserFactory};
use App\Utils\Writers\{WriterInterface, WriterFactory};
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\{InputInterface, InputArgument};
use Symfony\Component\Console\Output\OutputInterface;

class Commissions extends Command
{
    /** @var string */
    private $binResource;
    /** @var string */
    private $exchangeRatesResource;
    /** @var string */
    private $readerResource;
    /** @var string */
    private $writerResource;

    public function __construct(string $binResource, string $exchangeRatesResource, string $readerResource, string $writerResource)
    {
        $this->binResource = $binResource;
        $this->exchangeRatesResource = $exchangeRatesResource;
        $this->readerResource = $readerResource;
        $this->writerResource = $writerResource;

        parent::__construct('calculate:commissions');
    }

    protected function configure() : void
    {
        $this->setDescription('Get commissions from transactions')
            ->addArgument('name', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        $parser = $this->buildParserObject();
        $parser->setFileName($input->getArgument('name'));

        $service = new CommissionsService();
        $calculator = new CommissionsCalculator();
        $countryCodeDefiner = $this->buildCountryCodeDefinerObject();
        $rateDefiner = $this->buildRateDefinerObject();
        $writer = $this->buildWriterObject();

        $calculator->setCodeDefiner($countryCodeDefiner);
        $calculator->setRateDefiner($rateDefiner);
        $service->setCalculator($calculator);
        $service->setParser($parser);
        $service->setWriter($writer);

        $service->calculateCommissions();
    }

    /**
     * @return ParserInterface
     */
    private function buildParserObject() : ParserInterface
    {
        return (new ParserFactory($this->readerResource))->getParserObject();
    }

    /**
     * @return WriterInterface
     */
    private function buildWriterObject() : WriterInterface
    {
        return (new WriterFactory($this->writerResource))->getWriterClass();
    }

    /**
     * @return RatesInterface
     */
    private function buildRateDefinerObject() : RatesInterface
    {
        return (new ExchangeRatesFactory($this->exchangeRatesResource))->getExchangeClass();
    }

    /**
     * @return CountryCodeInterface
     */
    private function buildCountryCodeDefinerObject() : CountryCodeInterface
    {
        return (new CountryCodeFactory($this->binResource))->getCountryCodeDefinerClass();
    }
}
<?php

namespace Test\Utils\Commissions\Calculations;

use App\Utils\Commissions\Calculations\CommissionsCalculator;
use App\Utils\Commissions\EuroUnionCountryCodeList;
use PHPUnit\Framework\TestCase;
use Test\TestHelpers;

/**
 * @package Test\Utils\Commissions\Calculations
 * @group commissions-service
 */
class CommissionsCalculatorTest extends TestCase
{
    use TestHelpers;

    /** @var CommissionsCalculator */
    private $target;

    public function setUp() : void
    {
        $this->target = new CommissionsCalculator();
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetValueByKey() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'getValueByKey', [['key' => 'value'], 'key']);
        self::assertSame('value', $actual, 'Function "getValueByKey" in CommissionsCalculator does not work correctly');
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetValueByKeyIsInvalid() : void
    {
        $this->expectException('RuntimeException');
        $this->callProtectedMethod($this->target, 'getValueByKey', [['key' => 'value'], 'not_existing_key']);
    }

    public function testIsInEU() : void
    {
        $mock = $this->getMockBuilder(EuroUnionCountryCodeList::class)
            ->onlyMethods(['has'])
            ->getMock();

        $mock->expects(self::once())
            ->method('has')
            ->with(self::equalTo('codeCountry'));

        $mock->has('codeCountry');
    }

    /**
     * @throws \ReflectionException
     */
    public function testCalculateCommissionsInEU() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'calculateCommissions', [true, 100]);
        self::assertSame(1.00, $actual, 'Function "calculateCommissions" in CommissionsCalculator does not work correctly');
    }

    /**
     * @throws \ReflectionException
     */
    public function testCalculateCommissionsInNonEU() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'calculateCommissions', [false, 100]);
        self::assertSame(2.00, $actual, 'Function "calculateCommissions" in CommissionsCalculator does not work correctly');
    }

    /**
     * @throws \ReflectionException
     */
    public function testCalculateAmountInEur() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'calculateAmountInEur', [100.00, 1.25]);
        self::assertSame(80.00, $actual, 'Function "calculateAmountInEur" in CommissionsCalculator does not work correctly');
    }
}
<?php

namespace Tests\Utils\Commissions;

use App\Utils\Commissions\CommissionsService;
use PHPUnit\Framework\TestCase;
use stdClass;
use Test\TestHelpers;

/**
 * @package Tests\Utils\Commissions
 * @group commissions-service
 */
class CommissionsServiceTest extends Testcase
{
    use TestHelpers;

    public function testCalculateCommissions() : void
    {
        $commissionsServiceMock = new CommissionsService();

        $parserMock = $this->getMockBuilder(stdClass::class)
            ->setMethods(['parse', 'setFileName'])
            ->getMock();
        $this->setProtectedProperty($commissionsServiceMock, 'parser', $parserMock);
        $parserMock->setFileName('input.txt');

        $parserMock->expects(self::once())
            ->method('parse')
            ->willReturn([
                '{"bin":"45717360","amount":"100.00","currency":"EUR"}',
                '{"bin":"516793","amount":"50.00","currency":"USD"}'
            ]);

        $calculatorMock = $this->getMockBuilder(stdClass::class)
            ->setMethods(['calculate'])
            ->getMock();
        $this->setProtectedProperty($commissionsServiceMock, 'calculator', $calculatorMock);

        $calculatorMock->expects(self::exactly(2))
            ->method('calculate')
            ->withConsecutive(
                ['{"bin":"45717360","amount":"100.00","currency":"EUR"}'],
                ['{"bin":"516793","amount":"50.00","currency":"USD"}']
            );

        $writerMock = $this->getMockBuilder(stdClass::class)
            ->setMethods(['write'])
            ->getMock();
        $this->setProtectedProperty($commissionsServiceMock, 'writer', $writerMock);

        $writerMock->expects(self::exactly(2))
            ->method('write');

        $commissionsServiceMock->calculateCommissions();
    }
}
<?php

namespace Test\Commissions\CurrencyRateDefiners;

use App\Exceptions\InvalidResourceException;
use App\Utils\Commissions\CountryCodeDefiners\LookupBinList;
use PHPUnit\Framework\TestCase;
use Test\TestHelpers;

/**
 * @package Test\Commissions\CurrencyRateDefiners
 * @group commissions-service
 */
class LookupBinListTest extends TestCase
{
    use TestHelpers;

    /** @var LookupBinList */
    private $target;

    public function setUp() : void
    {
        $this->target = new LookupBinList();
    }

    /**
     * @param  int     $bin
     * @param  string  $expectedCountryCode
     *
     * @return void
     * @dataProvider getCountryCodeDataProvider
     */
    public function testGetCountryCodeResult(int $bin, string $expectedCountryCode) : void
    {
        $actual = $this->target->getCountryCode($bin);

        self::assertSame($expectedCountryCode, $actual, 'Function "getCountryCode" in LookupBinList does not work correctly');
    }

    /**
     * @return array
     */
    public function getCountryCodeDataProvider() : array
    {
        return [
            'DenmarkBin' => [
                'bin' => 45717360,
                'expectedCountryCode' => 'DK'
            ],
            'JapanBin' => [
                'bin' => 45417360,
                'expectedCountryCode' => 'JP'
            ]
        ];
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetInvalidResourceContent() : void
    {
        $this->expectException(InvalidResourceException::class);
        $this->callProtectedMethod($this->target, 'getResourceContent', ['11111111111']);
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetResourceContent() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'getResourceContent', ['45717360']);
        self::assertIsString(gettype($actual));
    }

    /**
     * @throws \ReflectionException
     */
    public function testFindCodeIsInvalid() : void
    {
        $this->expectException(InvalidResourceException::class);
        $this->callProtectedMethod($this->target, 'findCode', [[]]);
    }

    /**
     * @throws \ReflectionException
     */
    public function testFindCode() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'findCode', [
                [
                    'country' => [
                        'alpha2' => 'JP'
                    ]
                ]
            ]
        );
        self::assertSame('JP', $actual, 'Function "findCode" in LookupBinList does not work correctly');
    }
}
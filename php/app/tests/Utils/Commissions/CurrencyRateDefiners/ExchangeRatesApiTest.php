<?php

namespace Test\Utils\Commissions\CurrencyRateDefiners;

use App\Exceptions\InvalidResourceException;
use App\Utils\Commissions\CurrencyRateDefiners\ExchangeRatesApi;
use PHPUnit\Framework\TestCase;
use Test\TestHelpers;

/**
 * @package Test\Utils\Commissions\CurrencyRateDefiners
 * @group commissions-service
 */
class ExchangeRatesApiTest extends TestCase
{
    use TestHelpers;

    /** @var ExchangeRatesApi */
    private $target;

    public function setUp() : void
    {
        $this->target = new ExchangeRatesApi();
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetResourceContent() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'getResourceContent', ['JPY']);
        $this->assertIsString(gettype($actual));
    }

    /**
     * @throws \ReflectionException
     */
    public function testFindCourseIsInvalid() : void
    {
        $this->expectException(InvalidResourceException::class);
        $this->callProtectedMethod($this->target, 'findCourse', [[], 'JPY']);
        $this->callProtectedMethod($this->target, 'findCourse', [['rates' => []], 'AAA']);
    }

    /**
     * @throws \ReflectionException
     */
    public function testFindCourseNotEUR() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'findCourse',
            [['rates' => ['JPY' => 125.37]], 'JPY']);
        $this->assertSame(125.37, $actual, 'Function "findCourse" in ExchangeRatesApi does not work correctly');
    }

    /**
     * @throws \ReflectionException
     */
    public function testFindCourseEUR() : void
    {
        $actual = $this->callProtectedMethod($this->target, 'findCourse',
            [[], 'EUR']);
        $this->assertSame(1.00, $actual, 'Function "findCourse" in ExchangeRatesApi does not work correctly');
    }
}
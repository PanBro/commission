<?php

namespace Tests\Utils\Commissions;

use App\Utils\Commissions\EuroUnionCountryCodeList;
use PHPUnit\Framework\TestCase;

/**
 * @package Tests\Utils\Commissions
 * @group commissions-service
 */
class EuroUnionCountryCodeListTest extends TestCase
{
    /** @var EuroUnionCountryCodeList */
    private $target;

    public function setUp() : void
    {
        $this->target = new EuroUnionCountryCodeList();
    }

    /**
     * @param  string  $countryCode
     * @param  bool    $expected
     *
     * @dataProvider hasDataProvider
     */
    public function testHas(string $countryCode, bool $expected) : void
    {
        $actual = $this->target->has($countryCode);

        self::assertSame($expected, $actual, 'Function "has" in EuroUnionCountryCodeList does not work correctly');
    }

    /**
     * dataProvider for testHas
     * @return array
     */
    public function hasDataProvider() : array
    {
        return [
            'EUCountries' => [
                'countryCode' => 'NL',
                'expected' => true
            ],
            'nonEUCounties' => [
                'countryCode' => 'BY',
                'expected' => false
            ]
        ];
    }

    public function testListHasNoWrongValue() : void
    {
        self::assertFalse($this->target->has('XX'));
    }
}
